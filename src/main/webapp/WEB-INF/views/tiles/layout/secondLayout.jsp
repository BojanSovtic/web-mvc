<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><tiles:getAsString name="title"></tiles:getAsString></title>

<style>
.flex-container {
	display: flex;
    padding: 2rem;
}

#sidemenu {
  flex: 1;
}

#site-content {
 flex: 4;
}

header, footer {
  text-align: center;
}
</style>
</head>
<body>
  <header id="header">
    <tiles:insertAttribute name="header" />
  </header>

  <div class="flex-container">
    <section id="sidemenu">
      <tiles:insertAttribute name="menu" />
    </section>

    <section id="site-content">
      <tiles:insertAttribute name="body" />
    </section>
  </div>

  <footer id="footer">
    <tiles:insertAttribute name="footer" />
  </footer>
</body>
</html>