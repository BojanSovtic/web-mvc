<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>This is page for MANUFACTURE list.</h2>
<div>
  <h1>All MANUFACTURES</h1>
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>City code</th>
        <th>City name</th>
        <th>Details</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach var="m" items="${manufactures}">
        <tr>
          <td>${m.name}</td>
          <td>${m.cityDto.number}</td>
          <td>${m.cityDto.name}</td>
          <td><a href="${pageContext.request.contextPath}/manufacturer/details/id/${m.id}">Details</a></td>
        </tr>
      </c:forEach>
    </tbody>
  </table>
</div>
