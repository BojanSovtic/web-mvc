<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<p>This is page for MANUFACTURER DETAILS...</p>

<c:if test="${not empty information}">
  <div>${information}</div>
</c:if>
<c:if test="${not empty exception}">
  <div>${exception}</div>
</c:if>

<form:form modelAttribute="contactPersonDto">
  <div>Firstname:</div>
  <div>
    <form:input type="text" path="firstname" size="60" readonly="true" />
  </div>
  
  <div>Lastname:</div>
  <div>
    <form:input type="text" path="lastname" size="60" readonly="true" />
  </div>

  <div>Manufacturer:</div>
  <div>
    <form:input type="text" path="manufacturerDto.name" size="60" readonly="true" />
  </div>
  
  <div>City number:</div>
  <div>
    <form:input type="text" path="manufacturerDto.cityDto.number" size="60" readonly="true" />
  </div>
  
  <div>City name:</div>
  <div>
    <form:input type="text" path="manufacturerDto.cityDto.name" size="60" readonly="true" />
  </div>

  <div>
    <a href="${pageContext.request.contextPath}/contact-person/edit/${contactPersonDto.id}">Edit</a>
  </div>
</form:form>

<form:form action="${pageContext.request.contextPath}/contact-person/delete/${contactPersonDto.id}" modelAttribute="contactPersonDto" method="POST">
  <button>Delete</button>
</form:form>
