<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<p>This is page for Contact Person CONFIRM...</p>

<c:if test="${not empty information}">
  <div>${information}</div>
</c:if>
<c:if test="${not empty exception}">
  <div>${exception}</div>
</c:if>


<form:form action="${pageContext.request.contextPath}/contact-person/save" modelAttribute="contactPersonDto"  method="POST" >

  <form:hidden path="id" />
  <form:hidden path="manufacturerDto.id" />
  <form:hidden path="manufacturerDto.cityDto.number" />

  <div>Firstname:</div>
  <div>
    <form:input type="text" path="firstname" size="60" readonly="true" />
  </div>
  <div>
    <form:errors path="firstname" />
  </div>
  
  <div>Lastname:</div>
  <div>
    <form:input type="text" path="lastname" size="60" readonly="true" />
  </div>
  <div>
    <form:errors path="lastname" />
  </div>

  <div>Manufacturer:</div>
  <div>
    <form:input type="text" path="manufacturerDto.name" size="60" readonly="true" />
  </div>

  <div>
    <button>Confirm</button>
  </div>
</form:form>
