<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>This is page for Contact person list.</h2>
<div>
  <h1>Contact person list.</h1>
  <table>
    <thead>
      <tr>
        <th>Firtname</th>
        <th>Lastname</th>
        <th>Manufacturer</th>
        <th>Details</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach var="contactPerson" items="${contactPersons}">
        <tr>
          <td>${contactPerson.firstname}</td>
          <td>${contactPerson.lastname}</td>
          <td>${contactPerson.manufacturerDto.name}</td>
          <td><a href="${pageContext.request.contextPath}/contact-person/details/${contactPerson.id}">Details</a></td>
        </tr>
      </c:forEach>
    </tbody>
  </table>
</div>
