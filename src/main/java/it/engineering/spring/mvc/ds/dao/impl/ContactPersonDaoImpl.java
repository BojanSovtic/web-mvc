package it.engineering.spring.mvc.ds.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.engineering.spring.mvc.ds.dao.ContactPersonDao;
import it.engineering.spring.mvc.ds.entity.ContactPersonEntity;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class ContactPersonDaoImpl implements ContactPersonDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void save(ContactPersonEntity contactPersonEntity) throws Exception {
		entityManager.persist(contactPersonEntity);
	}

	@Override
	public ContactPersonEntity findbyId(Long id) throws Exception {
		return entityManager.find(ContactPersonEntity.class, id);
	}

	@Override
	public List<ContactPersonEntity> getAll() throws Exception {
		return entityManager
				.createQuery("SELECT cp FROM ContactPersonEntity cp ORDER BY lastname", ContactPersonEntity.class)
				.getResultList();
	}

	@Override
	public void delete(Long id) throws Exception {
		entityManager
			.createQuery("DELETE FROM ContactPersonEntity cp WHERE cp.id = :id")
			.setParameter("id", id)
			.executeUpdate();
	}

	@Override
	public ContactPersonEntity update(ContactPersonEntity contactPersonEntity) throws Exception {
		return entityManager.merge(contactPersonEntity);
	}

}
