package it.engineering.spring.mvc.ds.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import it.engineering.spring.mvc.ds.dto.ManufacturerDto;
import it.engineering.spring.mvc.ds.service.ManufacturerService;

public class ManufacturerDtoFormatter implements Formatter<ManufacturerDto> {
	private final ManufacturerService manufacturerService;
	
	@Autowired
	public ManufacturerDtoFormatter(ManufacturerService manufacturerService) {
		this.manufacturerService = manufacturerService;
	}

	@Override
	public String print(ManufacturerDto manufacturerDto, Locale locale) {
		return manufacturerDto.getName();
	}

	@Override
	public ManufacturerDto parse(String manufacturer, Locale locale) throws ParseException {
		try {
			return manufacturerService.findById(Long.parseLong(manufacturer));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ParseException("Greska u ManufacturerDtoFormatter-u!", 0);
		}
	}

}
