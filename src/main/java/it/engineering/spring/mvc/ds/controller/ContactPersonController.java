package it.engineering.spring.mvc.ds.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import it.engineering.spring.mvc.ds.dto.ContactPersonDto;
import it.engineering.spring.mvc.ds.dto.ManufacturerDto;
import it.engineering.spring.mvc.ds.service.ContactPersonService;
import it.engineering.spring.mvc.ds.service.ManufacturerService;
import it.engineering.spring.mvc.ds.validator.ContactPersonValidator;

@Controller
@RequestMapping(path = {"/contact-person", "/cp"})
public class ContactPersonController {
	private final ContactPersonService contactPersonService;
	private final ManufacturerService manufacturerService;
	
	@Autowired
	public ContactPersonController(ContactPersonService contactPersonService, ManufacturerService manufacturerService) {
		this.contactPersonService = contactPersonService;
		this.manufacturerService = manufacturerService;
	}
	
	@GetMapping(path = "/add")
	public ModelAndView add(HttpServletRequest request) throws Exception {
		ModelAndView modelAndView =  new ModelAndView("contact-person/add");
		
		String manufacturerId = request.getParameter("manufacturerId");
		
		if (manufacturerId != null) {
			ContactPersonDto contactPersonDto = new ContactPersonDto();
			contactPersonDto.setFirstname("Firstname");
			contactPersonDto.setLastname("Lastname");
			contactPersonDto.setManufacturerId(request.getParameter("manufacturerId"));
			modelAndView.addObject("contactPersonDto", contactPersonDto);
		}
	
		return modelAndView;
	}
	
	@GetMapping(path = "/edit/{id}")
	public ModelAndView edit(@PathVariable(name = "id") Long id) throws Exception {
		ContactPersonDto contactPersonDto = contactPersonService.findById(id);
		
		ModelAndView modelAndView = new ModelAndView("contact-person/edit");
		modelAndView.addObject("contactPersonDto", contactPersonDto);
		
		return modelAndView;
	}
	
	@PostMapping(path = "/save")
	public ModelAndView saveOrUpdate(
			@Valid @ModelAttribute(name = "contactPersonDto") ContactPersonDto contactPersonDto,
			Errors errors) throws Exception {
		
		ModelAndView modelAndView = new ModelAndView();
		String view = "contact-person/add";
		
		if (errors.hasErrors()) {
			if (contactPersonDto.getId() != null);
				view = "contact-person/edit";
		} else {
			if (contactPersonDto.getId() == null) {
				System.out.println(contactPersonDto);
				contactPersonService.save(contactPersonDto);
				view = "redirect:/contact-person/add";
			} else {
				contactPersonService.update(contactPersonDto);
				view = "redirect:/contact-person/details/" + contactPersonDto.getId();
			}
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
	}
	

	@PostMapping(path = "/confirm")
	public ModelAndView confirm(
			@Valid @ModelAttribute(name = "contactPersonDto") ContactPersonDto contactPersonDto,
			Errors errors) throws Exception {
		
		ModelAndView modelAndView = new ModelAndView();
		String view = "contact-person/add";
		
		ManufacturerDto manufacturerDto = manufacturerService.findById(Long.valueOf(contactPersonDto.getManufacturerId()));
		modelAndView.addObject("manufacturerDto", manufacturerDto);
		
		if (errors.hasErrors()) {
			if (contactPersonDto.getId() != null);
				view = "contact-person/edit";
		} else {
			view = "contact-person/confirm";
		}
		
		modelAndView.setViewName(view);
		return modelAndView;
	}
	
	@GetMapping(path = "/details/{id}")
	public ModelAndView details(@PathVariable(name = "id") Long id) throws Exception {
		ModelAndView modelAndView = new ModelAndView("contact-person/details");
		modelAndView.addObject("contactPersonDto", contactPersonService.findById(id));
		
		return modelAndView;
	}
	
	@GetMapping(path = "/list")
	public ModelAndView list() throws Exception {
		ModelAndView modelAndView = new ModelAndView("contact-person/list");
		modelAndView.addObject("contactPersons", contactPersonService.getAll());
		return modelAndView;
	}
	
	@PostMapping(path = "/delete/{id}")
	public ModelAndView delete(@PathVariable(name = "id") Long id) throws Exception {
		contactPersonService.delete(id);
		
		return new ModelAndView("redirect:/contact-person/list");
	}
	
	@ModelAttribute(name = "contactPersonDto")
	private ContactPersonDto contactPersonDto() {
		ContactPersonDto contactPersonDto = new ContactPersonDto();
		contactPersonDto.setFirstname("Firstname");
		contactPersonDto.setLastname("Lastname");
		return contactPersonDto;
	}
	
	@ModelAttribute(name = "manufacturers")
	private List<ManufacturerDto> getManufacturers() throws Exception {
		return manufacturerService.getAll();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		System.out.println(">>> initBinder()");
		if (binder.getTarget() instanceof ContactPersonDto)
			binder.addValidators(new ContactPersonValidator());
	}

}
