package it.engineering.spring.mvc.ds.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;

import org.hibernate.tool.schema.internal.exec.ScriptSourceInputNonExistentImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.engineering.spring.mvc.ds.converter.impl.ContactPersonConverterDtoEntity;
import it.engineering.spring.mvc.ds.dao.ContactPersonDao;
import it.engineering.spring.mvc.ds.dao.ManufacturerDao;
import it.engineering.spring.mvc.ds.dto.ContactPersonDto;
import it.engineering.spring.mvc.ds.entity.ContactPersonEntity;
import it.engineering.spring.mvc.ds.entity.ManufacturerEntity;
import it.engineering.spring.mvc.ds.exception.ExistEntityException;
import it.engineering.spring.mvc.ds.service.ContactPersonService;

@Service
@Transactional
public class ContactPersonServiceImpl implements ContactPersonService {
	private final ContactPersonDao contactPersonDao;
	private final ManufacturerDao manufacturerDao;
	private final ContactPersonConverterDtoEntity contactPersonConverterDtoEntity;
	
	@Autowired
	public ContactPersonServiceImpl(ContactPersonDao contactPersonDao, ManufacturerDao manufacturerDao,
			ContactPersonConverterDtoEntity contactPersonConverterDtoEntity) {
		this.contactPersonDao = contactPersonDao;
		this.manufacturerDao = manufacturerDao;
		this.contactPersonConverterDtoEntity = contactPersonConverterDtoEntity;
	}

	@Override
	public void save(ContactPersonDto contactPersonDto) throws Exception {
		if (contactPersonDto.getId() != null) {
			throw new ExistEntityException(contactPersonDto, "Can't save ContactPerson, id present!");
		}
		
		ManufacturerEntity manufacturerEntity = manufacturerDao.findbyId(contactPersonDto.getManufacturerDto().getId());
		if (manufacturerEntity == null) {
			throw new ExistEntityException(manufacturerEntity, "Can't save ContactPerson, manufacturer doesn't exist!");
		}
		
		ContactPersonEntity contactPersonEntity = contactPersonConverterDtoEntity.toEntity(contactPersonDto);
		contactPersonEntity.setManufacturer(manufacturerEntity);
		
		contactPersonDao.save(contactPersonEntity);
	}

	@Override
	public List<ContactPersonDto> getAll() throws Exception {
		List<ContactPersonEntity> entities = contactPersonDao.getAll();
		return entities.stream().map(entity -> {
			return contactPersonConverterDtoEntity.toDto(entity);
		}).collect(Collectors.toList());
	}

	@Override
	public ContactPersonDto findById(Long id) throws Exception {
		ContactPersonEntity contactPersonEntity = contactPersonDao.findbyId(id);
		
		if (contactPersonEntity == null)
			throw new ExistEntityException(contactPersonEntity, "Contact person doesn't exist!");
		
		return contactPersonConverterDtoEntity.toDto(contactPersonEntity);
	}

	@Override
	public void delete(Long id) throws Exception {
		contactPersonDao.delete(id);
	}

	@Override
	public ContactPersonDto update(ContactPersonDto contactPersonDto) throws Exception {
		ContactPersonEntity contactPersonEntity = contactPersonDao.findbyId(contactPersonDto.getId());
		if (contactPersonEntity == null)
			throw new ExistEntityException(contactPersonEntity, "Contact person doesn't exist!");
		
		ManufacturerEntity manufacturerEntity = manufacturerDao.findbyId(contactPersonDto.getManufacturerDto().getId());
		if (manufacturerEntity == null)
			throw new ExistEntityException(manufacturerEntity, "Manufacturer for Contact person doesn't exist!");
		
		contactPersonEntity = contactPersonConverterDtoEntity.toEntity(contactPersonDto);
		contactPersonEntity.setManufacturer(manufacturerEntity);
		
		contactPersonEntity = contactPersonDao.update(contactPersonEntity);
		
		return contactPersonConverterDtoEntity.toDto(contactPersonEntity);
	}
	
	
}
