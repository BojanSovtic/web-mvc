package it.engineering.spring.mvc.ds.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class ContactPersonDto implements Serializable, Dto {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String firstname;
	private String lastname;
	private ManufacturerDto manufacturerDto;

	public ContactPersonDto() {
	}

	public ContactPersonDto(Long id, String firstname, String lastname, ManufacturerDto manufacturerDto) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.manufacturerDto = manufacturerDto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public ManufacturerDto getManufacturerDto() {
		return manufacturerDto;
	}

	public void setManufacturerDto(ManufacturerDto manufacturerDto) {
		this.manufacturerDto = manufacturerDto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactPersonDto other = (ContactPersonDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ContactPersonDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", manufacturerDto=" + manufacturerDto + "]";
	}

}
