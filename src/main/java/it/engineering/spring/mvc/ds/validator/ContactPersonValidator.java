package it.engineering.spring.mvc.ds.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.engineering.spring.mvc.ds.dto.ContactPersonDto;

public class ContactPersonValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ContactPersonDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		System.out.println(">>> ContactPersonValidator - validate");
		ContactPersonDto contactPersonDto = (ContactPersonDto) target;
		if (contactPersonDto != null) {
			if (contactPersonDto.getFirstname().isEmpty()) {
				errors.rejectValue("firstname", "contactPerson.firstname",
						"Firstname is required!");
			}
			if (contactPersonDto.getLastname().isEmpty()) {
				errors.rejectValue("lastname", "contactPerson.lastname",
						"Lastname is required!");
			}
			if (contactPersonDto.getManufacturerDto() == null) {
				errors.rejectValue("manufacturerDto", "contactPerson.manufacturerDto",
						"Manufacturer is required!");
			}
		}
	}

}
